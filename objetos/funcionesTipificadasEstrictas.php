<?php
declare(strict_types=1);
Class Operacion{
    public $nombre;
}


function sumar(int $a,int $b,Operacion $c){
    return $a+$b;
}


echo sumar(2,5,new Operacion()); // 7

// echo sumar(2,5,"sumar"); // Fatal error el tercer argumento debe ser un objeto de tipo operacion

echo sumar(2,3.6,new Operacion()); // Fatal error espera que el numero b sea entero





