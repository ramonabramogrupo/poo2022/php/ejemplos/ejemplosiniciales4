<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$ford=new Coche("ford","mondeo",2000);

// esto produce error
//$ford->marca="Fiat";

$ford->setMarca("Fiat");

var_dump($ford);

$ford->setModelo("doblo");

var_dump($ford);

$ford->cilindrada=1000;
$ford->setCilindrada(1000);

var_dump($ford);