<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

$alumno=new Humano("andres", "Masculino", "2000/1/1");
$profesor=new Humano("Jose", "masculino", "2000/1/1");

echo $alumno->presentarse();
echo $profesor->presentarse();


$oficio1=new Oficio("profesor",20, 30);
$oficio2=new Oficio("administrativo", 25, 50);

echo "<br>";
echo $oficio1->calcular();
echo "<br>";
echo $oficio2->calcular();


$trabajan1=new Trabajan($profesor,$oficio1);
$trabajan2=new Trabajan($alumno,$oficio2);

var_dump($trabajan1,$trabajan2);

echo "<br>el nombre del profesor es {$trabajan1->persona->nombre}";
echo "<br>el nombre del profesor es {$profesor->nombre}";
