<?php

class Humano {
    public $nombre;
    public $sexo;
    public $fechaNacimiento;
    
    public function __construct($nombre, $sexo, $fechaNacimiento) {
        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->fechaNacimiento = $fechaNacimiento;
    }
    
    public function presentarse(){
        return "<br>{$this->nombre} {$this->fechaNacimiento}";
    }

   
}
