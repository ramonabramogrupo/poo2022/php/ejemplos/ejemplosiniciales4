<?php

class Oficio {
    public $nombre;
    public $salarioBase; //precio por hora
    public $horasSemanales;
    
    public function __construct($nombre, $salarioBase, $horasSemanales) {
        $this->nombre = $nombre;
        $this->salarioBase = $salarioBase;
        $this->horasSemanales = $horasSemanales;
    }
    
    public function calcular(){
        return $this->salarioBase*$this->horasSemanales;
    }
}
