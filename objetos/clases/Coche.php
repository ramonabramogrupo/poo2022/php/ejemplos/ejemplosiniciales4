<?php

class Coche {
    private $marca;
    private $modelo;
    public $cilindrada;

    public function __construct($marca, $modelo, $cilindrada) {
        $this->marca = $marca;
        $this->modelo = $modelo;
        $this->cilindrada = $cilindrada;
    }
    
    public function setMarca($marca): void {
        $this->marca = $marca;
    }

    public function setModelo($modelo): void {
        $this->modelo = $modelo;
    }

    public function setCilindrada($cilindrada): void {
        $this->cilindrada = $cilindrada;
    }


}
