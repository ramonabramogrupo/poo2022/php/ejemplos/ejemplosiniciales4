<?php

class Persona {
    /** miembros **/
    
    // propiedad
    public $nombre;
    public $apellidos;
    public $direccion;
    public $poblacion;
    public $cp;
    
    // metodo 
    public function hablar(){
        return "bla bla bla";
    }
    
    public function direccion(){
        return "{$this->direccion} {$this->poblacion} {$this->cp}";
    }
}

