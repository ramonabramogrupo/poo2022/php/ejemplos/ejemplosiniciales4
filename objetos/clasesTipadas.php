<?php

class Direccion{
    public int $calle;
    public int $numero;
    public int $piso;
    public int $puerta;
}

class Persona{
    public string $nombre;
    public int $edad;
    public Direccion $direccion;
}

$p=new Persona();
$p->nombre="Ramon";
$p->edad=30.85; // almacena 30
//$p->direccion="Calle Sol"; // Fatal error





