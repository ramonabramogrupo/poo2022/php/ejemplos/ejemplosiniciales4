<?php

class Direccion{
    public int $calle;
    public int $numero;
    public int $piso;
    public int $puerta;
}

class Persona{
    public string $nombre;
    public int $edad;
    public Direccion $direccion;
    public function __construct(string $nombre, int $edad, Direccion $direccion) {
        $this->nombre = $nombre;
        $this->edad = $edad;
        $this->direccion = $direccion;
    }

}

$p=new Persona("ramon",30.85,new Direccion());









