<?php

class Persona{
    /** miembros **/
    // propiedades
    public $nombre;
    private $grado;
    
    //metodos
    public function hablar(){
        return "bla bla bla";
    }
    
    public function hablarAlto(){
        // llamar a un miembro desde dentro de la clase
         return $this->chillar(); 
    }

    private function chillar(){
        return "ah...";
    }
    
}

/** crear un objeto de tipo persona */

$persona1=new Persona();

$persona1->nombre="Pepe"; // escribiendo un valor en nombre
echo $persona1->nombre; // accediendo al nombre
echo $persona1->hablar(); // llamando al metodo hablar

/** no.....**/
//Persona->nombre="aaa";
//Persona->hablar();

echo $persona1->hablarAlto();

