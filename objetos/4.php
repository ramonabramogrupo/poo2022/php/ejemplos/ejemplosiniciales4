<?php
// autocarga de clases
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . '.php';
});

// crear objeto de tipo persona
$persona1=new Persona();

// colocando un valor en la propiedad nombre
$persona1->nombre="pepe";
// leyendo la propiedad nombre
echo $persona1->nombre;

echo $persona1->hablar();

$persona1->apellidos="Vazquez";
$persona1->direccion="Vargas 1 5D";
$persona1->poblacion="Santander";
$persona1->cp="39005";

echo $persona1->direccion();


$persona2=new Persona();
$persona2->nombre="ana";
$persona2->direccion="Augusto 3";
$persona2->poblacion="Torrelavega";
$persona2->cp="39002";

echo "<br>";
echo $persona2->nombre;
echo "<br>";
echo $persona2->direccion();

// crear un objeto de tipo perro
$perro1=new Perro("Tarzan");

// leo el nombre del perro
echo "<br>";
echo $perro1->nombre;

$perro2=new Perro();


// crear un objeto de tipo gato
$gato1=new Gato("busi","negro","si",10);

// leer las propiedades del gato1
echo "<br>";
echo "Mi gato se llama " . $gato1->nombre;
echo "<br>";
echo "El color de mi gato es " . $gato1->color;

$gato2=new Gato("Dulci");
var_dump($gato2);