<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            require './_formulario7.php';
            
            // comprobar si he pulsado alguno 
            // de los botones
            if(isset($_GET["operacion"])){
                $numero1=$_GET["numero1"];
                $numero2=$_GET["numero2"];
                // compruebo si el numero1 es menor que
                // el numero2
                if($numero1<$numero2){
                    switch ($_GET["operacion"]){
                        case 'ascendente':
                            for($c=$numero1;$c<=$numero2;$c++){
                                echo "<li>{$c}</li>";
                            }
                            break;
                        case 'descendente':
                            for($c=$numero2;$c>=$numero1;$c--){
                                echo "<li>{$c}</li>";
                            }
                            break;
                    }       
                    
                }else{
                    echo "numero1 debe ser menor que el numero2 ";
                }
                
            }
        ?>
    </body>
</html>
