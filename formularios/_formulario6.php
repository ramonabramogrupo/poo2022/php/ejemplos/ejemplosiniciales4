<form>
    <div>
        <label for="numero1">Numero 1</label>
        <input type="number" id="numero1" name="numero1" required value="<?= $numero1 ?>" placeholder="introduce numero"/>
    </div>
    <div>
        <label for="numero2">Numero 2</label>
        <input type="number" id="numero2" name="numero2" required value="<?= $numero2 ?>" placeholder="introduce numero">
    </div>
    <div>
        <button name="operacion" value="sumar">Sumar</button>
        <button name="operacion" value="restar">Restar</button>
        <button name="operacion" value="multiplicar">Multiplicar</button>
        <button name="operacion" value="dividir">Dividir</button>
    </div>
</form>
<div>
    <?= $resultado ?>
</div>