<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // inicializo las variables
        $numero1="";
        $numero2="";
        $resultado="";
                
        // comprobar si he pulsado alguno de los
        // botones
        
        if(isset($_GET["operacion"])){
            $numero1=$_GET["numero1"];
            $numero2=$_GET["numero2"];
        
            // ¿Que boton has pulsado?
            switch($_GET["operacion"]){
               case 'sumar':
                   $resultado=$numero1+$numero2;
                   break;
               case 'restar':
                   $resultado=$numero1-$numero2;
                   break;
               case 'multiplicar':
                   $resultado=$numero1*$numero2;
                   break;
               case 'dividir':
                   $resultado=$numero1/$numero2;
                   break;
            }
            
        }
        
        require './_formulario6.php';
        
?>
    </body>
</html>
