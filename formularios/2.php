<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // inicializo las variables
        
        $nombre="";
        $edad="";
        $poblacion="";
                
        // compruebo si he pulsado enviar
        if (isset($_GET["boton"])) {
            //extract($_GET); // esta funcion me crea una variable por cada indice del array
            $nombre = $_GET["nombre"];
            $edad = $_GET["edad"];
            $poblacion = $_GET["poblacion"];
        }
        ?>

        <form>
            <div>
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" id="nombre"/>
            </div>
            <div>
                <label for="edad">Edad</label>
                <input type="number" name="edad" id="edad"/>
            </div>
            <div>
                <label for="poblacion">Poblacion</label>
                <input type="text" name="poblacion" id="poblacion"/>
            </div>
            <div>
                <button name="boton">Enviar</button>
            </div>

        </form>

        <table>
            <tr>
                <td>Nombre</td>
                <td><?= $nombre ?></td>
            </tr>
            <tr>
                <td>Edad</td>
                <td><?= $edad ?></td>
            </tr>
            <tr>
                <td>Poblacion</td>
                <td><?= $poblacion ?></td>
            </tr>
        </table>
    </body>
</html>
