<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // inicializo las variables

        $nombre = "";
        $edad = "";
        $poblacion = "";

        // compruebo si he pulsado enviar
        if (isset($_GET["boton"])) {
            //extract($_GET); // esta funcion me crea una variable por cada indice del array
            $nombre = $_GET["nombre"];
            $edad = $_GET["edad"];
            $poblacion = $_GET["poblacion"];
            // muestro el resultado
            require './_tabla3.php';
        } else {
            // cargar el formulario cuando todavia no he pulsado enviar
            require './_formulario3.php';
        }
        ?>

    </body>
</html>
